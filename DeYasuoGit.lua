if not myHero.charName == "Yasuo" then return end

local SweepBladeKey = string.byte("G")

local qCount = 0
local castQforSweep = false
local dashPos = {}
local towers = {}






	enemyMinions = minionManager(MINION_ENEMY, 1300, myHero, MINION_SORT_HEALTH_DES)

function OnLoad()
	PrintChat("DeYasuo By Raimen12: Big tnx to Hex for helping :) ")
	YasuoConfig = scriptConfig("DeYasuo - GodMode", "deyasuo")
	YasuoConfig:addParam("SwipeAll", "Use q on swipe", SCRIPT_PARAM_ONOFF, false)
	YasuoConfig:addParam("IgnoreTowers", "Should it ignore them?", SCRIPT_PARAM_ONOFF, false)
	YasuoConfig:addParam("YosuoSweep", "Yosuo autoSweep", SCRIPT_PARAM_ONKEYDOWN, false, SweepBladeKey)
	YasuoConfig:permaShow("YosuoSweep")

	
	towersUpdate()
end	
		



		
function OnDraw()
	DrawCircle(myHero.x, myHero.y, myHero.z, 285, 0x00FF00)	
	DrawCircle(myHero.x, myHero.y, myHero.z, 475, 0x0000FF)	
end
		
function JumpPred(from,to,range)
	return from + (Vector(to) - from):normalized()*range
end
		
		
function OnTick()
	enemyMinions:update()
	if YasuoConfig.YosuoSweep then killAllWithSweep() end
	if YasuoConfig.SwipeAll then swipeThemAll() end
end


		
function OnSendPacket(packet)
	local decodedPacket = Packet(packet)
	if decodedPacket.header == Packet.headers.E_CAST then
		if decodedPacket:get('spellId') == _E then
			dashPos.x = myHero.x
			dashPos.y = myHero.y
			dashPos.z = myHero.z
			castQforSweep = true
			
		end
	end
end
		
function swipeThemAll()
	if castQforSweep and GetDistance(dashPos,myHero)>200 then

		if myHero:CanUseSpell(_Q) == READY then
			CastSpell(_Q, myHero.x+20,myHero.z+30)
			castQforSweep = false
		else
			castQforSweep = false
		end
			
		
	end
end	

function haveYDW(tgt)
	buffCount = tgt.buffCount
	for i = 1, buffCount, 1 do
		local buff = tgt:getBuff(i)
		if buff.valid and buff.name == "YasuoDashWrapper" then
			return true
		end
	end
	return false
end
 
function towersUpdate()
        for i = 1, objManager.iCount, 1 do
                local obj = objManager:getObject(i)
                if obj and obj.type == "obj_AI_Turret" and obj.health > 0 then
                        if not string.find(obj.name, "TurretShrine") and obj.team ~= player.team then
                                table.insert(towers, obj)
                        end
                end
        end
end
 
function inTurretRange(pos)
        if YasuoConfig.IgnoreTowers then return false end
        local check = false
        for i, tower in ipairs(towers) do
                if tower and (tower.health > 0 or not tower.dead) then
                        if GetDistance(tower, pos) <= 810 then check = true end
                else
                        table.remove(towers, i)
                end
        end
        return check
end
		
		
		
function killAllWithSweep()
	if myHero:CanUseSpell(_E) == READY then
		for i, minion in pairs(enemyMinions.objects) do
				if minion ~= nil and not minion.dead then
					local distanceToMinion = GetDistance(minion)
					if distanceToMinion < 475 and not haveYDW(minion) and not inTurretRange(JumpPred(myHero,minion,475)) then
						CastSpell(_E, minion)
						break
					end
				end
			end
	end
end
		
function killWithSweep()
	if  myHero:CanUseSpell(_E) == READY then
		for i, minion in pairs(enemyMinions.objects) do
			if minion ~= nil and not minion.dead then			
				local distanceToMinion = GetDistance(minion)
				if distanceToMinion < 475 then
					local sweepDmg = getDmg("E", minion, myHero)-5
					if minion.health < sweepDmg then
						CastSpell(_E, minion)
						if myHero:CanUseSpell(_Q) == READY then
							CastSpell(_E, minion)
							break
						end
					end
				end
			end
		end
	end
end	
		